<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Welcome to Foundation | Workspace</title>
  <link rel="stylesheet" href="./css/css/global.css">
  <!-- <script src="js/vendor/custom.modernizr.js"></script> -->
  <!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->

  <script src="/js/jquery-1.8.3.min.js" type="text/javascript"></script>
  <script src="/js/jquery.colorbox-min.js" type="text/javascript"></script>
  <script src="/js/jquery.bxslider.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>

</head>
<body class="<?php echo $classes; ?>">
<div id="wrap">

<!--===
Header
====-->
<section id="header">
  
  <!--========
  Top Section
  =========-->
  <section class="top row">

    <div class="social-icons columns large-4 small-6">
      <a class="facebook" href="#"></a>
      <a class="twitter" href="#"></a>
      <a class="google" href="#"></a>
      <a class="instagram" href="#"></a>
    </div><!-- .social-icons -->

    <div class="mobile-icons columns hide-for-medium-up small-6 awesome-icon">
      <a class="search" href="#">&#xf002;</a>
      <a class="th-list" href="#">&#xf00b;</a>
    </div>

    <div class="columns show-for-small small-12">
      <hr>
    </div>
    

    <div class="logo columns large-4 small-12">
      <a href="/">
        <h2>Charleston Palmetto Card</h2>
      </a>
    </div><!-- .logo -->

    <div class="small-purchase columns small-12 hide-for-medium-up">
      <a class="blue-button" href="/cart"><span class="l"></span><span class="m">Purchase</span><span class="r"></span></a>
    </div>

    <div class="searchbar columns large-4 hide-for-small">
      <form class="searchbar">
        <input type="search" placeholder="Search">
        <input type="submit" value="&#xf002;">
      </form>      
    </div><!-- .searchbar -->

  </section><!-- .top.row -->




  <!--======
  Main Nav
  =======-->
  <div class="row">

    <nav class="main-menu columns large-12 hide-for-small">

      <ul>

        <!-- Primary Nav Item -->
        <li class="active primary">
          <a href="/category">Services</a>

          <ul class="subnav" style="left: -70px;">
            <div class="drop-arrow" style="left: 84px;"></div>
            <li>
              <a href="/subcategory">Automotive</a>
            </li>
            <li>
              <a href="/subcategory">Computer &amp; Office</a>
            </li>
            <li>
              <a href="/subcategory">Health &amp; Wellness</a>
            </li>
            <li>
              <a href="/subcategory">Home Improvement</a>
            </li>
          </ul><!-- .subnav -->

        </li><!-- .primary -->

        <!-- Primary Nav Item -->
        <li class="primary">
          <a href="/category">Dining</a>

          <ul class="subnav" style="left: -68px;">
            <div class="drop-arrow"></div>
            <li>
              <a href="/subcategory">Category 1</a>
            </li>
            <li>
              <a href="/subcategory">Category 2</a>
            </li>
            <li>
              <a href="/subcategory">Category 3</a>
            </li>
            <li>
              <a href="/subcategory">Category 4</a>
            </li>
          </ul><!-- .subnav -->

        </li><!-- .primary -->

        <!-- Primary Nav Item -->
        <li class="primary">
          <a href="/category">Entertainment</a>

          <ul class="subnav" style="left: -42px;">
            <div class="drop-arrow"></div>
            <li>
              <a href="/subcategory">Dropdown 1</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 2</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 3</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 4</a>
            </li>
          </ul><!-- .subnav -->

        </li><!--.primary -->

        <!-- Primary Nav Item -->
        <li class="purchase primary nosub">
          <a class="blue-button" href="/cart"><span class="l"></span><span class="m">Purchase</span><span class="r"></span></a>
        </li><!-- .primary -->

        <!-- Primary Nav Item -->
        <li class="primary">
          <a href="/category">Shopping</a>

          <ul class="subnav" style="left: -58px;">
            <div class="drop-arrow"></div>
            <li>
              <a href="/subcategory">Dropdown 1</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 2</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 3</a>
            </li>
            <li>
              <a href="/subcategory">Dropdown 4</a>
            </li>
          </ul><!-- .subnav -->
        </li><!-- primary li -->

        <!-- Primary Nav Item -->
        <li class="primary">
          <a href="/about">About</a>

          <ul class="subnav" style="left: -68px;">
            <div class="drop-arrow"></div>
            <li>
              <a href="/about-us">About Us</a>
            </li>
            <li>
              <a href="/faq">FAQ</a>
            </li>
            <li>
              <a href="/contact">Contact</a>
            </li>
          </ul><!-- .subnav -->

        </li><!-- .primary -->

        <!-- Primary Nav Item -->
        <li class="primary nosub">
          <a href="/activate">Activate</a>
        </li><!-- .primary -->

      </ul>
    </nav><!-- .main-menu -->

  </div>


  <!--<a class="toggle-mobile-nav"></a>-->

</section><!-- #header.row -->

<section class="page-template">

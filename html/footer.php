</section><!-- .template -->

<footer id="footer">
  <div class="row">
    <section class="large-6 small-12 columns copyright">
      <p class="copyright">
        &copy;2013 by Charleston Palmetto Card. 
      </p>
    </section><!--  -->

    <section class="large-6 small-12 columns social-buttons">
      <a class="facebook" href="#"></a>
      <a class="twitter" href="#"></a>
      <a class="google" href="#"></a>   
      <a class="instagram" href="#"></a>  
    </section>
  </div>
</footer>
</div><!-- #wrap --> 

<?php
  echo_html( 'html/templates/popup-detail.html' );
?>

</body>
</html>
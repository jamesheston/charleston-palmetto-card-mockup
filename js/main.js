var SITE = ( function( $ ){

  var
  pub = {},

  //private properties
  bknd;

  pub.init = function(){
    //listeners, etc.
    run_sitewide_functions();
    run_current_template_functions();
  }

  pub.run_template_functions_index = function(){
    console.log( 'running index template functions' );
  }

  //---------------------------------------------
  // Category and Subcategory Template Functions
  //---------------------------------------------
  pub.run_template_functions_category = function(){

    //-------------------
    // Homepage Carousel
    //-------------------
    ( function(){
      
      var
      $carousel = $( '.bxslider' ).bxSlider( {
        mode: 'fade',
        pager: true,
        speed: 1000,
        auto: true,
        pause: 8000
      } ),

      bknd;

    } )();       

    //---------------------------
    // Featured tiles pagination
    //---------------------------
    ( function(){

      var
      $tile_gallery,
      $prev_button,
      $next_button
      bknd;

      $( '#parent' ).on( 'click', '#target', function( event ){
        var $this = $( this );
        
      });
      
      
    } )();// featured tiles pagination
  }

  //--------------------------------
  // Subcategory Template Functions
  //--------------------------------
  pub.run_template_functions_subcategory = function(){
    pub.run_template_functions_category();
  }

  //------------------------
  // FAQ Template Functions
  //------------------------
  pub.run_template_functions_faq = function(){  console.log( 'running faq template functions' );

    //------------------------
    // Accordion Interactions
    //------------------------
    var
    root_selector = 'section.faq ul',
    item_selector = root_selector + ' li',

    $accordion = $( root_selector ),
    $items = $( item_selector ),
    bknd;

    // toggle open/closed on click
    $( root_selector ).on( 'click', 'li .question', function( event ){

      var 
      $target_item = $( $(this).parent( 'li' ) ),
      bknd;

      // close
      if( $target_item.hasClass( 'active' ) ){
        toggle_collapse( $target_item );
      }
      // open after closing previously active item
      else{
        toggle_collapse( 
          $( item_selector + '.active' ), 
          $target_item 
        );
      }
    });

    function toggle_collapse( $close_target, $open_target ){

      var
      $close_content,
      $open_content,
      bknd;

      if( $close_target ){
        $close_content = $( $close_target.children( '.answer' ) );
      }
      if( $open_target ){
        $open_content = $( $open_target.children( '.answer' ) );
      }
      
      // handles all situations except when there are no active items left
      $close_content.slideUp( function(){
        $close_target.removeClass( 'active' );

        if( $open_target ){
          $open_content.slideDown( function(){
            $open_target.addClass( 'active' );
          } );          
        }
      } );
      // handles an item being clicked when there are no active items/all content is collapsed
      if( $open_content ){
        $open_content.slideDown( function(){
          $open_target.addClass( 'active' );
        } );              
      }

    }// function toggle_collapse(){ ... }

  }// pub.run_template_functions_faq(){ ... }

  return pub;

  function run_sitewide_functions(){

    console.log( 'running sitewide functions' );

    //-------------------
    // Open Detail Popup
    //-------------------
    $( '.all-items.psuedo-table li > a' ).click( function( e ){
      e.preventDefault();

      var
      $source = $( '.popup-src' ),
      $clone = $source.clone(),
      bknd;

      $clone.removeClass( 'popup-src' );
      $clone.addClass( 'detail-popup' );
      $clone.show();

      $.colorbox( {
        html: $clone
      } );

    } );
  }

  function run_current_template_functions(){
    var
    current_template = $( 'body' ).attr( 'class' ),
    function_name = 'run_template_functions_' + current_template;

    if( window[ "SITE" ].hasOwnProperty(function_name) ){
      window[ "SITE" ][ function_name ]();
    }
  }

}( jQuery ) );// module definition

$( document ).ready( function(){
    
  SITE.init();

} );// doc ready
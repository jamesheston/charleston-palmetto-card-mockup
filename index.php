<?php
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');
 
$path = 'http://' . $_SERVER['SERVER_NAME'] . '/';
// if it doesn't contain the string 'local', append '/sites/demo...'
define( 'ROOT_URL', $path );
define( 'HTML_URL', getcwd() . '/html/' );
define( 'IMG_URL', $path . 'img/' );

//var_dump( $_GET );

$template = get_template_name( $_GET['request'] );

$classes = get_body_classes( $template );

output_page( $template, $classes );


function output_page( $template, $classes ){
  include( getcwd() . '/php/functions.php' );
  ob_start();
  include(  getcwd() . '/html/header.php' );
  $header = ob_get_clean();

  ob_start();
  include(  getcwd() . '/html/templates/' . $template . '.html' );
  $template = ob_get_clean();

  ob_start();
  include(  getcwd() . '/html/footer.php' );
  $footer = ob_get_clean();

  $html =  "{$header} {$template} {$footer}";

  echo $html;
}

function get_template_name( $template ){
  
  $matches = array(  
    ''                 => 'index',
    'deal-detail'      => 'deal-detail',
    'subcategory'      => 'subcategory',
    'category'         => 'category',
    'cart'             => 'cart',
    'about-us'         => 'about-us',
    'about'         => 'about-us',
    'activate'         => 'activate',
    'contact'          => 'contact',
    'faq'              => 'faq',
  );

  
  $match = $matches[ $template ];
  return $match;
  
}


function get_body_classes( $template ){
  $matches = array(
    'index'             => 'index',
    'deal-detail'       => 'deal-detail',
    'subcategory'       => 'subcategory',
    'category'          => 'category',
    'cart'              => 'cart',
    'about-us'          => 'about-us',
    'activate'          => 'activate',
    'contact'           => 'contact',
    'faq'               => 'faq',
  );

  $match = $matches[ $template ];

  return $match;
}

?>
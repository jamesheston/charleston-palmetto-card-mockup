<?php 

function echo_html( $file_path ){

  ob_start();
  include( $file_path );
  $html = ob_get_clean();
  echo $html;

}

?>
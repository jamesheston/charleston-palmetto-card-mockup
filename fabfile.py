from fabric.api import *
import os

proj_dir = os.path.dirname(__file__)
repo_dir = os.path.dirname(proj_dir)
# production_hosts = ['root@fs2.sinelabs.com',]
# env.hosts = production_hosts

def push(msg): # called like: fab push:'commit message'
  local( 'git add -A' )
  local( 'git status' )
  local( 'git commit -a -m "' + msg + '"'  )
  local( 'git push' )
